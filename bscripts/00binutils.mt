let name = "binutils-xcompiled";
let version = [2,43];
building::download_extract("Binutils", "binutils-2.43", ".tar.gz", "https://ftp.gnu.org/gnu/binutils/binutils-2.43.tar.gz", "025c436d15049076ebe511d29651cc4785ee502965a8839936a65518582bdd64");
building::set_env("CC", "clang");
building::step("Create tools directory", "mkdir", bdir() + "tools");
building::step("Configure Binutils", "./configure", "--prefix=/tmp/tgt_os --with-sysroot=/tmp/tgt_os/tools --target=x86_64-unknown-linux-musl --disable-nls --enable-gprofng=no --disable-werror --enable-new-dtags --enable-default-hash-style=gnu");
building::step("Build", "make", "");
building::step("Install", "make", "install");
building::unset_env("CC");
let name = "musl-xcompiled";
let version = [1,2,5];
building::download_extract("MUSL", "musl-1.2.5", ".tar.gz", "https://musl.libc.org/releases/musl-1.2.5.tar.gz", "a9a118bbe84d8764da0ea0d28b3ab3fae8477fc7e4085d90102b8596fc7c75e4");
building::set_env("CC", "clang -static");
building::step("Configure", "./configure",  "--prefix=/tmp/tgt_os");
building::step("Build", "make", "");
building::step("Install", "make", "install TARGET=x86_64-linux-musl");
building::unset_env("CC");
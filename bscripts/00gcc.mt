let name = "gcc";
let version = "14.2.0";
building::download_extract("GCC", name+"-"+version, ".tar.gz", "https://ftp.gnu.org/gnu/gcc/gcc-14.2.0/gcc-14.2.0.tar.gz", "7d376d445f93126dc545e2c0086d0f647c3094aae081cdb78f42ce2bc25e7293");
building::set_env("CC", "clang");
building::set_env("OS", "/tmp/tgt_os");
building::step("Create build directory", "mkdir", "build");
building::step("Change to it", "cd", "build");
building::step("Configure it", "../configure", "\
    --target=$OS              \
    --prefix=$OS/tools        \
    --with-glibc-version=2.40 \
    --with-sysroot=$OS        \
    --with-newlib             \
    --without-headers         \
    --enable-default-pie      \
    --enable-default-ssp      \
    --disable-nls             \
    --disable-shared          \
    --disable-multilib        \
    --disable-threads         \
    --disable-libatomic       \
    --disable-libgomp         \
    --disable-libquadmath     \
    --disable-libssp          \
    --disable-libvtv          \
    --disable-libstdcxx       \
    --enable-languages=c,c++");
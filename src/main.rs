#![warn(clippy::perf, clippy::all, clippy::pedantic)]
use log::info;
use std::{
    env::{set_current_dir, temp_dir},
    fs::{copy, create_dir_all, read_dir, remove_dir_all, remove_file},
    os::unix::fs::symlink,
    path::Path,
    process::Command,
    thread::spawn,
};
use xdg_home::home_dir;
/*plan:
    set up file structure
    bootstrap clang, binutils and busybox
    set up chroot via arch_chroot
    run commands inside the chroot
*/
fn main() {
    colog::init();
    Command::new("figlet").arg("MatuushOS").status().unwrap();
    info!("MatuushOS system builder");
    let chroot = Path::new(&temp_dir()).join("tgt_os");
    if chroot.exists() {
        remove_dir_all(&chroot).unwrap();
        info!("Cleaned previous iteration of MatuushOS chroot");
    }
    for dir in ["mtos/pkgs", "mtos/cfg", "mtos/users", "mtos/stuff"] {
        let path = chroot.join(dir);
        create_dir_all(&path).unwrap();
        info!("Created directory {}", path.display());
    }
    let sym_d: Vec<(&str, &str)> = vec![
        ("bin", "mtos/pkgs"),
        ("etc", "mtos/cfg"),
        ("home", "mtos/users"),
        ("usr", "mtos/stuff"),
    ];
    set_current_dir(&chroot).unwrap();
    for sym in sym_d {
        symlink(sym.1, sym.0).unwrap();
        info!("Symlinked {} into {}", sym.1, sym.0);
    }
    set_current_dir(env!("CARGO_MANIFEST_DIR")).unwrap();
    info!("Downloading pm to speed up eficiency");
    let link: &str = "target/pmc";
    if Path::new(link).exists() {
        remove_file(link).unwrap();
    }
    Command::new("cargo")
        .args(["install", "--git", "https://gitlab.com/MatuushOS/pm"])
        .status()
        .unwrap();
    symlink(Path::new(&home_dir().unwrap()).join(".cargo/bin/pm"), link).unwrap();
    for scr in read_dir(Path::new(env!("CARGO_MANIFEST_DIR")).join("bscripts")).unwrap() {
        let fname2 = scr.unwrap();
        let fname = fname2.path();
        let after_split: Vec<_> = fname.to_str().unwrap().split(".mt").collect();
        info!("{}", after_split[0]);
        let cmd = Command::new(link)
            .args(["run", after_split[0]])
            .status()
            .unwrap();
        spawn(move || cmd);
    }
}
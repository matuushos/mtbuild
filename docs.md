# building

```Namespace: global/building```

<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> copy_local </h2>

```rust
fn copy_local(source: String, dest: String)
```

<div>
<div class="tab">
<button group="copy_local" id="link-copy_local-Description"  class="tablinks active" 
    onclick="openTab(event, 'copy_local', 'Description')">
Description
</button>
<button group="copy_local" id="link-copy_local-Arguments"  class="tablinks" 
    onclick="openTab(event, 'copy_local', 'Arguments')">
Arguments
</button>
<button group="copy_local" id="link-copy_local-Example"  class="tablinks" 
    onclick="openTab(event, 'copy_local', 'Example')">
Example
</button>
</div>

<div group="copy_local" id="copy_local-Description" class="tabcontent"  style="display: block;" >
Copies a file from one location to another on the local system.
</div>
<div group="copy_local" id="copy_local-Arguments" class="tabcontent"  style="display: none;" >

* `source` - The source file path on the local system.
* `dest` - The destination file path on the local system.
</div>
<div group="copy_local" id="copy_local-Example" class="tabcontent"  style="display: none;" >

```rust
use pm::functions::building::copy_local;
copy_local("/local/path/file.txt", "/new/path/file.txt");
```
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> download </h2>

```rust
fn download(name: String, file_name: String, ext: String, addr: String, sha256: String)
```

<div>
<div class="tab">
<button group="download" id="link-download-Description"  class="tablinks active" 
    onclick="openTab(event, 'download', 'Description')">
Description
</button>
</div>

<div group="download" id="download-Description" class="tabcontent"  style="display: block;" >
Only downloads the given file.
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> download_extract </h2>

```rust
fn download_extract(name: String, file_name: String, ext: String, addr: String, sha256: String)
```

<div>
<div class="tab">
<button group="download_extract" id="link-download_extract-Description"  class="tablinks active" 
    onclick="openTab(event, 'download_extract', 'Description')">
Description
</button>
</div>

<div group="download_extract" id="download_extract-Description" class="tabcontent"  style="display: block;" >
Downloads and extracts the target file.
For only downloading or downloading binaries, use `download()` instead.
EXTRACT THE COMPRESSED FILE BEFOREHAND TO SEE IF ANY DIRECTORY CHANGE IS NEEDED.
- if so, use the [`download`] function instead in combination with [`step`].
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> install </h2>

```rust
fn install(pkg_name: String)
```

<div>
<div class="tab">
<button group="install" id="link-install-Description"  class="tablinks active" 
    onclick="openTab(event, 'install', 'Description')">
Description
</button>
</div>

<div group="install" id="install-Description" class="tabcontent"  style="display: block;" >
Installs the package.
Sets the INSTDIR environment variable for easy putting.
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> mkpackage </h2>

```rust
fn mkpackage(name: String)
```

<div>
<div class="tab">
<button group="mkpackage" id="link-mkpackage-Description"  class="tablinks active" 
    onclick="openTab(event, 'mkpackage', 'Description')">
Description
</button>
</div>

<div group="mkpackage" id="mkpackage-Description" class="tabcontent"  style="display: block;" >
Packages the contents in the pkg directory.
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> remote_step </h2>

```rust,ignore
fn remote_step(host: String, cmd: String, args: String) -> ExitStatus
```

<div>
<div class="tab">
<button group="remote_step" id="link-remote_step-Description"  class="tablinks active" 
    onclick="openTab(event, 'remote_step', 'Description')">
Description
</button>
<button group="remote_step" id="link-remote_step-Arguments"  class="tablinks" 
    onclick="openTab(event, 'remote_step', 'Arguments')">
Arguments
</button>
<button group="remote_step" id="link-remote_step-Returns"  class="tablinks" 
    onclick="openTab(event, 'remote_step', 'Returns')">
Returns
</button>
<button group="remote_step" id="link-remote_step-Example"  class="tablinks" 
    onclick="openTab(event, 'remote_step', 'Example')">
Example
</button>
</div>

<div group="remote_step" id="remote_step-Description" class="tabcontent"  style="display: block;" >
Executes a command on a remote system over SSH.
</div>
<div group="remote_step" id="remote_step-Arguments" class="tabcontent"  style="display: none;" >

* `host` - The remote host to connect to.
* `cmd` - The command to run on the remote host.
* `args` - A string of arguments to pass to the command.
</div>
<div group="remote_step" id="remote_step-Returns" class="tabcontent"  style="display: none;" >

Returns the exit status of the remote command.
</div>
<div group="remote_step" id="remote_step-Example" class="tabcontent"  style="display: none;" >

```rust,no_run
use pm::functions::remote::remote_step;
let status = remote_step("user@example.com", "ls", "-l /home");
```
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> remote_step_unary </h2>

```rust,ignore
fn remote_step_unary(host: String, cmd: String, args: String) -> int
```

<div>
<div class="tab">
<button group="remote_step_unary" id="link-remote_step_unary-Description"  class="tablinks active" 
    onclick="openTab(event, 'remote_step_unary', 'Description')">
Description
</button>
<button group="remote_step_unary" id="link-remote_step_unary-Arguments"  class="tablinks" 
    onclick="openTab(event, 'remote_step_unary', 'Arguments')">
Arguments
</button>
<button group="remote_step_unary" id="link-remote_step_unary-Examples"  class="tablinks" 
    onclick="openTab(event, 'remote_step_unary', 'Examples')">
Examples
</button>
</div>

<div group="remote_step_unary" id="remote_step_unary-Description" class="tabcontent"  style="display: block;" >
Runs a remote command on a specified host and returns the termination signal of the command.
</div>
<div group="remote_step_unary" id="remote_step_unary-Arguments" class="tabcontent"  style="display: none;" >

* `host`: The hostname and IP address of the remote machine.
* `cmd`: The command to execute on the remote machine.
* `args`: Arguments to pass to the command.

returns: i32 - The signal number that caused the command to terminate
</div>
<div group="remote_step_unary" id="remote_step_unary-Examples" class="tabcontent"  style="display: none;" >

```rust,no_run
use pm::functions::building::remote_step_unary;
let signal = remote_step_unary("user@example.com", "ls", "-la");
println!("Terminated with signal: {}", signal);
```
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> set_env </h2>

```rust,ignore
fn set_env(env: String, var: String)
```

<div>
<div class="tab">
<button group="set_env" id="link-set_env-Description"  class="tablinks active" 
    onclick="openTab(event, 'set_env', 'Description')">
Description
</button>
</div>

<div group="set_env" id="set_env-Description" class="tabcontent"  style="display: block;" >
Sets the variable
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> step </h2>

```rust,ignore
fn step(name: String, cmd: String, args: String) -> ExitStatus
```

<div>
<div class="tab">
<button group="step" id="link-step-Description"  class="tablinks active" 
    onclick="openTab(event, 'step', 'Description')">
Description
</button>
</div>

<div group="step" id="step-Description" class="tabcontent"  style="display: block;" >
The function that runs commands
</div>

</div>
</div>
</br>
<div style='box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding: 15px; border-radius: 5px; border: 1px solid var(--theme-hover)'>
    <h2 class="func-name"> <code>fn</code> unset_env </h2>

```rust,ignore
fn unset_env(env: String)
```

<div>
<div class="tab">
<button group="unset_env" id="link-unset_env-Description"  class="tablinks active" 
    onclick="openTab(event, 'unset_env', 'Description')">
Description
</button>
</div>

<div group="unset_env" id="unset_env-Description" class="tabcontent"  style="display: block;" >
Unsets the variable
</div>

</div>
</div>
</br>


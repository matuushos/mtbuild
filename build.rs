fn main() {
    #[cfg(target_os = "windows")]
	compile_error!("Native Windows is not supported, because this crate is only meant for Unix-like systems like Linux. \
	\nNO MSVC SUPPORT ON THE WAY BTW. 
	\nEnable WSL to compile this program or this will keep failing")
}
